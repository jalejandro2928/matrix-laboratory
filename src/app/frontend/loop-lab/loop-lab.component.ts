import { Component, OnInit, OnDestroy, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WOW } from 'wowjs/dist/wow.min';


@Component({
  selector: 'app-loop-lab',
  templateUrl: './loop-lab.component.html',
  styleUrls: ['./loop-lab.component.scss']
})
export class LoopLabComponent implements OnInit, OnDestroy, AfterViewInit {

  isNavbarCollapsed = true;
  delponeImageUrl = '../../../assets/background/accounts.jpg';
  lastDisplacement: number = null;
  navBarState = 'normal';
  noInversa = false;
  wow = new WOW({ live: false });

  generateMatrix = false;
  generateInverseMatrix = false;
  matrix: any[] = [];
  tempMatrix: any[] = [];
  result: any[] = [];
  dimension = 2;

  ContactUsForm: FormGroup;
  constructor(private fb: FormBuilder) {

  }

  ngOnInit() {
    this.ContactUsForm = this.fb.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      message: [null, Validators.required]
    });

    window.addEventListener('scroll', this.scrollEvent, true);
  }

  ngAfterViewInit() {
    this.wow.init();
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scrollEvent, true);
  }


  /////// Event Scrol Handle //////

  scrollEvent = (event: any): void => {
    const displacement = window.scrollY;
    if (this.lastDisplacement === null) {
      this.lastDisplacement = displacement;
      this.navBarState = 'normal';
    } else if (this.lastDisplacement < displacement) {
      this.navBarState = 'downNav';
    } else if (this.lastDisplacement > displacement) {
      this.navBarState = 'normal';
    }
    this.lastDisplacement = displacement;
  }

  ///////////////////////////////////////////////////////////////
  onGenerarMatrix(): void {
    this.matrix = [];
    this.tempMatrix = [];
    for (let i = 0; i < this.dimension; i++) {
      this.matrix.push(new Array(this.dimension).fill(1));
      this.tempMatrix.push(new Array(this.dimension).fill(1));
    }
    this.generateMatrix = true;
  }


  onChangeInput(value, row, col): void {
    let i = row + 0;
    let j = col + 0;
    this.tempMatrix[i][j] = value;
  }

  onGenerateInverse() {
    console.log(this.tempMatrix);
    this.noInversa = false;
    this.generateInverseMatrix = false;
    this.calcularInversa(JSON.parse(JSON.stringify(this.tempMatrix)));
    this.result.map(row => {
      row.map(items => {
        if (items === null || items === undefined) {
          this.noInversa = true;
        }

      });
    });
    this.generateInverseMatrix = true;

  }

  calcularInversa(matrix): void {
    const numberColumns = matrix[0].length;

    matrix = this.extendToIdentity(matrix);
    for (let i = 0; i < numberColumns; i++) {
      matrix = this.findPivot(matrix, i, i);
      matrix = this.convertTo0Rows(matrix, i, i);
    }
    matrix = matrix.map(function (row) {
      return row.splice(numberColumns, numberColumns);
    });
    this.result = JSON.parse(JSON.stringify(matrix));
  }


  ////////// Funciones para calcular la inversa de una matrix/////////
  ////////// Se aplica el metodo de Jordan  //////////////

  extendToIdentity(matrix) {
    const numberColumns = matrix[0].length;
    const numberRows = matrix.length;
    const n = numberColumns;
    matrix.map(row => {
      if (row.length !== numberColumns || (row.length !== numberRows)) {
        throw new Error('La matrix no es cuadrada');
      }
    });

    for (let index = 0; index < matrix.length; index++) {
      let row = matrix[index];
      const tempRowIdentity = [];
      for (let i = 0; i < n; i++) {
        tempRowIdentity.push((i === index) ? 1 : 0);
      }
      row = row.concat(tempRowIdentity);
      matrix[index] = row;
    }
    return JSON.parse(JSON.stringify(matrix));
  }
  ///// Encuentra el pivote /////////////
  findPivot(matrix, columnIndex, rowBundary) {
    let findOne = false;
    let rowIndex = -1;
    for (let i = rowBundary; i < matrix.length; i++) {
      if (matrix[i][columnIndex] === 1) {
        findOne = true;
        rowIndex = i;
        break;
      }
    }
    if (findOne) {
      const temp = JSON.parse(JSON.stringify(matrix[rowBundary]));
      matrix[rowBundary] = JSON.parse(JSON.stringify(matrix[rowIndex]));
      matrix[rowIndex] = JSON.parse(JSON.stringify(temp));
    } else {
      const c = matrix[rowBundary][columnIndex] + 0;
      for (let i = columnIndex; i < matrix[rowBundary].length; i++) {
        matrix[rowBundary][i] = matrix[rowBundary][i] / c;
      }
    }
    return JSON.parse(JSON.stringify(matrix));
  }

  //////Elimina de la columna en que me encuentro todas las filas////
  convertTo0Rows(matrix, columnIndex, rowBundary) {
    const pivot = matrix[rowBundary][columnIndex] + 0;
    const n = matrix[0].length;
    for (let i = 0; i < matrix.length; i++) {
      if (i !== rowBundary) {
        const a = matrix[i][columnIndex] + 0;
        const factor = pivot * -1.0 * a;
        for (let j = columnIndex; j < n; j++) {
          matrix[i][j] = matrix[i][j] + matrix[rowBundary][j] * factor;
        }
      }
    }
    return JSON.parse(JSON.stringify(matrix));
  }




}




