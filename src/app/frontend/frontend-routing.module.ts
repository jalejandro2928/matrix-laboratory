
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoopLabComponent } from './loop-lab/loop-lab.component';

const routes: Routes = [
  {
    path: '',
    component: LoopLabComponent,
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontendRoutingModule { }
