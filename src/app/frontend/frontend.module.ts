import { AppShareModule } from './../app-share/app-share.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrontendRoutingModule } from './frontend-routing.module';
import { LoopLabComponent } from './loop-lab/loop-lab.component';


@NgModule({
  declarations: [
    LoopLabComponent
  ],
  imports: [
    CommonModule,
    AppShareModule,
    FrontendRoutingModule
  ]
})
export class FrontendModule { }
