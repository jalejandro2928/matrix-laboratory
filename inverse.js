var GlobalMatrix = [[1, 5, 1], [1, 1, 1], [2, 8, -150]];

function calcularInversa(matrix) {
    var numberColumns = matrix[0].length;

    matrix = extendToIdentity(matrix);
    for (var i = 0; i < numberColumns; i++) {
        matrix = findPivot(matrix, i, i);
        matrix = convertTo0Rows(matrix, i, i);
    }
    matrix = matrix.map(function (row) {
        return row.splice(numberColumns, numberColumns);
    });
    showMatrix(matrix);
}


calcularInversa(GlobalMatrix);


////////// Funciones para calcular la inversa de una matrix/////////
////////// Se aplica el metodo de Jordan  //////////////

function extendToIdentity(matrix) {
    var numberColumns = matrix[0].length;
    var numberRows = matrix.length;
    var n = numberColumns;
    matrix.map(row => {
        if (row.length !== numberColumns || (row.length !== numberRows)) {
            throw new Error('La matrix no es cuadrada');
        }
    });

    for (var index = 0; index < matrix.length; index++) {
        var row = matrix[index];
        var tempRowIdentity = [];
        for (var i = 0; i < n; i++) {
            tempRowIdentity.push((i === index) ? 1 : 0);
        }
        row = row.concat(tempRowIdentity);
        matrix[index] = row;
    }
    return JSON.parse(JSON.stringify(matrix));
}
///// Encuentra el pivote /////////////
function findPivot(matrix, columnIndex, rowBundary) {
    var findOne = false;
    var rowIndex = -1;
    for (var i = rowBundary; i < matrix.length; i++) {
        if (matrix[i][columnIndex] === 1) {
            findOne = true;
            rowIndex = i;
            break;
        }
    }
    if (findOne) {
        var temp = JSON.parse(JSON.stringify(matrix[rowBundary]));
        matrix[rowBundary] = JSON.parse(JSON.stringify(matrix[rowIndex]));
        matrix[rowIndex] = JSON.parse(JSON.stringify(temp));
    }
    else {
        var c = matrix[rowBundary][columnIndex] + 0;
        for (var i = columnIndex; i < matrix[rowBundary].length; i++) {
            matrix[rowBundary][i] = matrix[rowBundary][i] / c;
        }
    }
    return JSON.parse(JSON.stringify(matrix));
}

//////Elimina de la columna en que me encuentro todas las filas////
function convertTo0Rows(matrix, columnIndex, rowBundary) {
    var pivot = matrix[rowBundary][columnIndex] + 0;
    var n = matrix[0].length;
    for (var i = 0; i < matrix.length; i++) {
        if (i !== rowBundary) {
            var a = matrix[i][columnIndex] + 0;
            var factor = pivot * -1.0 * a;
            for (var j = columnIndex; j < n; j++) {
                matrix[i][j] = matrix[i][j] + matrix[rowBundary][j] * factor;
            }
        }
    }
    return JSON.parse(JSON.stringify(matrix));
}

/////Muestra la matrix///////////
function showMatrix(matrix) {
    for (var i = 0; i < matrix.length; i++) {
        console.log(matrix[i]);
    }
}
